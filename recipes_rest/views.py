from types import SimpleNamespace

from rest_framework import viewsets, permissions, mixins, status, generics
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.views import APIView

import json

from rest_framework_simplejwt.tokens import RefreshToken

from recipes_rest.permissions import IsOwnerOrReadOnly
from recipes_rest.serializers import *


class RecipeViewSet(viewsets.ModelViewSet):
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer
    permission_classes = (IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset()
        search_name = self.request.query_params.get('name', None)
        if search_name:
            names = queryset.filter(name__icontains=search_name)
            queryset &= names
        search_category = self.request.query_params.get('category', None)
        if search_category:
            categories = queryset.filter(category__id=search_category)
            queryset &= categories
        search_creator = self.request.query_params.get('creator', None)
        if search_creator:
            creator = queryset.filter(creator=search_creator)
            queryset &= creator
        search_favorited = self.request.query_params.get('favoured_by', None)
        if search_favorited:
            profile = Profile.objects.get(user=search_favorited)
            queryset &= profile.favorite_recipes.all() | Recipe.objects.filter(creator=search_favorited)
        search_ingredients = self.request.query_params.getlist('includedIngredients', [])
        for ingredient in search_ingredients:
            quantities = Quantity.objects.filter(ingredient=ingredient)
            recipe_ids = quantities.values_list('moment__recipe__id', flat=True)
            recipes = Recipe.objects.filter(id__in=recipe_ids)
            queryset &= recipes
        excluded_ingredients = self.request.query_params.getlist('excludedIngredients', [])
        for ingredient in excluded_ingredients:
            quantities = Quantity.objects.filter(ingredient=ingredient)
            recipe_ids = quantities.values_list('moment__recipe__id', flat=True)
            recipes = Recipe.objects.exclude(id__in=recipe_ids)
            queryset &= recipes
        return queryset


class TimeUnitViewSet(mixins.ListModelMixin,
                      viewsets.GenericViewSet):
    queryset = TimeUnit.objects
    serializer_class = TimeUnitSerializer


class RecipeIngredients(mixins.ListModelMixin,
                        mixins.UpdateModelMixin,
                        viewsets.GenericViewSet):
    queryset = Quantity.objects
    serializer_class = QuantitySerializer
    lookup_field = 'ingredient__moment__recipe__id'


class OwnedRecipeViewSet(mixins.ListModelMixin,
                         viewsets.GenericViewSet):
    queryset = Recipe.objects
    serializer_class = RecipeSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        profile = Profile.objects.get(user=user)
        recipes = profile.favorite_recipes.all()
        return recipes.distinct()


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)


class SmallRecipesViewSet(RecipeViewSet):
    serializer_class = SmallRecipeSerializer


class MyProfileView(mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ProfileSerializer

    def get_object(self):
        profile = Profile.objects.get(user=self.request.user)
        return profile


class QuantityViewSet(mixins.CreateModelMixin,
                      mixins.RetrieveModelMixin,
                      mixins.DestroyModelMixin,
                      mixins.UpdateModelMixin,
                      viewsets.GenericViewSet):
    queryset = Quantity.objects.all()
    serializer_class = QuantitySerializer
    permission_classes = (IsOwnerOrReadOnly,)


class MomentViewSet(viewsets.ModelViewSet):
    queryset = Moment.objects.all()
    serializer_class = MomentSerializer
    permission_classes = (IsOwnerOrReadOnly,)


class SmallMomentViewSet(viewsets.ModelViewSet):
    queryset = Moment.objects.all()
    serializer_class = SmallMomentSerializer
    permission_classes = (IsOwnerOrReadOnly,)


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class IngredientViewSet(viewsets.ModelViewSet):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)


class UserViewSet(viewsets.ReadOnlyModelViewSet, generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        refresh = RefreshToken.for_user(SimpleNamespace(id=serializer.data.get("id")))

        return Response({
            'refresh': str(refresh),
            'token': str(refresh.access_token),
        }, status=status.HTTP_201_CREATED, headers=headers)


class UploadView(APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        recipe_data = json.loads(request.data['json'])
        recipe_serializer = RecipeSerializer(data=recipe_data)
        recipe_serializer.is_valid(raise_exception=True)
        file = None
        if request.FILES:
            dict = {
                'url': request.FILES['file']
            }
            file_serializer = FileSerializer(data=dict)
            file_serializer.is_valid(raise_exception=True)
            file = file_serializer.save()
        recipe_serializer.save(file=file, creator=self.request.user)
        headers = UploadView.get_success_headers(recipe_serializer.data)
        return Response(recipe_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @staticmethod
    def get_success_headers(data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}


class FoodPlanViewSet(viewsets.ModelViewSet):
    serializer_class = PlannedFoodSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)

    def get_queryset(self):
        day = PlannedDay.objects.filter(date__gte=date.today())
        week_ids = day.values_list('week__id', flat=True)
        return FoodPlanning.objects.filter(id__in=week_ids).distinct()

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owners=[self.request.user])
