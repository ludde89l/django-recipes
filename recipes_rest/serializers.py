from django.db import transaction
from rest_framework import serializers, validators

from recipes_models.models import *


class EnumField(serializers.Field):

    def to_internal_value(self, data):
        return DisplayType(data)

    def to_representation(self, obj):
        return obj.value if obj else None


class FileSerializer(serializers.ModelSerializer):

    url = serializers.FileField(source='file')

    class Meta:
        model = File
        fields = ('id', 'url', 'timestamp')


class IdAndNameSerializer(serializers.ModelSerializer):

    name = serializers.CharField(validators=[validators.UniqueValidator(queryset=Ingredient.objects.all(),
                                                                        message='En ingrediens med det namnet finns redan')],
                                 write_only=True,
                                 required=False)
    translated_name = serializers.CharField(read_only=True, source='name')
    id = serializers.IntegerField(required=False)


class TimeUnitSerializer(IdAndNameSerializer):

    class Meta:
        model = TimeUnit
        fields = ('id', 'name', 'translated_name',)


class IngredientSerializer(IdAndNameSerializer):

    class Meta:
        model = Ingredient
        fields = ('id', 'name', 'translated_name',)


class CategorySerializer(IdAndNameSerializer):

    class Meta:
        model = Category
        fields = ('id', 'name', 'translated_name',)


class QuantitySerializer(serializers.ModelSerializer):
    ingredient = IngredientSerializer()
    amount = serializers.FloatField()

    class Meta:
        model = Quantity
        fields = ('amount', 'unit', 'ingredient', 'comment',)


class SmallQuantitySerializer(serializers.ModelSerializer):
    amount = serializers.FloatField()

    class Meta:
        model = Quantity
        fields = ('amount', 'unit', 'ingredient', 'comment',)


class ProfileSerializer(serializers.ModelSerializer):
    display_type = EnumField()

    class Meta:
        model = Profile
        fields = ('favorite_recipes', 'display_type',)


class EnumSerializer(serializers.Serializer):
    display_type = EnumField()

    class Meta:
        model = Profile
        fields = ('favorite_recipes', 'display_type',)


class MomentSerializer(serializers.ModelSerializer):
    ingredients = QuantitySerializer(source='quantity_set', many=True)
    extra_ingredients = IngredientSerializer(many=True)

    class Meta:
        model = Moment
        fields = ('id', 'name', 'ingredients', 'extra_ingredients', 'instructions', )


class SmallMomentSerializer(serializers.ModelSerializer):
    ingredients = SmallQuantitySerializer(source='quantity_set', many=True)

    class Meta:
        model = Moment
        fields = ('id', 'name', 'ingredients', 'extra_ingredients', 'instructions', 'recipe')


class RecipeSerializer(serializers.ModelSerializer):

    creator = serializers.ReadOnlyField(source='creator.username')
    moments = MomentSerializer(many=True)
    time_unit = TimeUnitSerializer()
    category = CategorySerializer(many=True)
    file = FileSerializer(read_only=True)

    @transaction.atomic
    def create(self, validated_data, commit=True, *args, **kwargs):
        moments = validated_data.pop('moments')
        json_categories = validated_data.pop('category')
        time_unit_id = validated_data.pop('time_unit')['id']
        time_unit = TimeUnit.objects.get(id=time_unit_id)
        recipe = Recipe.objects.create(**validated_data, time_unit=time_unit)
        for json_category in json_categories:
            recipe.category.add(Category.objects.get(id=json_category['id']))
        for json_moment in moments:
            quantities = json_moment.pop('quantity_set')
            extra_ingredients = json_moment.pop('extra_ingredients')
            moment = Moment.objects.create(recipe=recipe, **json_moment)
            for ingredient in extra_ingredients:
                moment.extra_ingredients.add(Ingredient.objects.get(id=ingredient['id']))
            for quantity in quantities:
                json_ingredient = quantity['ingredient']
                quantity['ingredient'] = Ingredient.objects.get(id=json_ingredient['id'])
                Quantity.objects.create(moment=moment, **quantity)
            moment.save()
        recipe.save()
        return recipe

    class Meta:
        model = Recipe
        fields = ('id', 'name', 'description', 'creator', 'time', 'time_unit', 'category', 'moments', 'portions', 'file', 'source')


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password')


class SmallRecipeSerializer(serializers.ModelSerializer):
    file = serializers.CharField(read_only=True)

    class Meta:
        model = Recipe
        fields = ('id', 'name', 'description', 'creator', 'time', 'time_unit', 'category', 'portions', 'file', 'source')


class PlannedDishSerializer(serializers.HyperlinkedModelSerializer):
    recipe = SmallRecipeSerializer()
    meal_name = serializers.CharField(source='get_meal_display', read_only=True)

    def create(self, validated_data):
        recipe = validated_data.pop('recipe')
        instance = PlannedRecipe(recipe=recipe['id'], **validated_data)
        instance.save()
        return instance

    class Meta:
        model = PlannedRecipe
        fields = ('recipe', 'meal', 'meal_name')


class PlannedDaySerializer(serializers.ModelSerializer):
    meals = PlannedDishSerializer(source='plannedrecipe_set', many=True)

    def create(self, validated_data):
        meals = validated_data.pop('plannedrecipe_set', [])
        day = PlannedDay(**validated_data)
        day.save()
        for meal in meals:
            meal['day'] = day
            PlannedDishSerializer().create(meal)
        day.save()
        return day

    def validate_meals(self, value):
        if not value:
            raise serializers.ValidationError("Must have at least one meal in a day")
        return value

    class Meta:
        model = PlannedDay
        fields = ('date', 'meals')


class PlannedFoodSerializer(serializers.ModelSerializer):
    days = PlannedDaySerializer(source='plannedday_set', many=True)
    start_date = serializers.DateField(read_only=True)
    end_date = serializers.DateField(read_only=True)

    def create(self, validated_data):
        days = validated_data.pop('plannedday_set', [])
        owners = validated_data.pop('owners', [])
        plan = FoodPlanning(**validated_data)
        plan.save()
        plan.owners.set(owners)
        plan.save()
        for day in days:
            day['week'] = plan
            PlannedDaySerializer().create(day)
        return plan

    def validate_days(self, value):
        if not value:
            raise serializers.ValidationError("Must have at least one day in the plan")
        return value

    class Meta:
        model = FoodPlanning
        fields = ('id', 'start_date', 'end_date', 'days')
