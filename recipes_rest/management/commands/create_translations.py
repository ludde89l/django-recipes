from recipes_models.models import Ingredient, TimeUnit, Category
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        handle = open('recipes_rest/texts.py','w')
        handle.write("from django.utils.translation import gettext\n")
        handle.write("\n")
        handle.write("\n")
        for ingredient in Ingredient.objects.all():
            handle.write('gettext("' + ingredient.name + '")\n')
        handle.write("\n")
        for time_unit in TimeUnit.objects.all():
            handle.write('gettext("' + time_unit.name + '")\n')
        handle.write("\n")
        for category in Category.objects.all():
            handle.write('gettext("' + category.name + '")\n')
        handle.close()