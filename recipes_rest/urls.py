from django.urls import re_path, include
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

from recipes_rest.views import *

router = routers.DefaultRouter()
router.register(r'recipes', RecipeViewSet, basename='recipe')
router.register(r'quantities', QuantityViewSet, basename='quantity')
router.register(r'ingredients', IngredientViewSet, basename='ingredient')
router.register(r'users', UserViewSet, basename='user')
router.register(r'categories', CategoryViewSet, basename='category')
router.register(r'moments', MomentViewSet, basename='moment')
router.register(r'profile', ProfileViewSet, basename='profile')
router.register(r'ownedRecipes', OwnedRecipeViewSet, basename='owned-recipes')
router.register(r'times', TimeUnitViewSet, basename='times')
router.register(r'recipeQuantities', RecipeIngredients, basename='recipe_quantities')
router.register(r'foodPlanning', FoodPlanViewSet, basename='food_plan')

small_router = routers.DefaultRouter()
small_router.register(r'recipes', SmallRecipesViewSet, basename='small-recipe')
small_router.register(r'moments', SmallMomentViewSet, basename='small-moment')

urlpatterns = [
    re_path(r'^', include(router.urls)),
    re_path(r'^small/', include(small_router.urls)),
    re_path(r'^upload/$', UploadView.as_view(), name='add-recipe'),
    re_path(r'^myProfile/', MyProfileView.as_view({'get': 'retrieve', 'post': 'update', 'patch': 'partial_update'}), name='my-profile'),
    re_path(r'^recipeIngredients/(?P<recipe_id>[0-9]+)/', RecipeIngredients.as_view({'get': 'list'}), name='recipe-ingredients'),
    re_path(r'^api-token-auth/', TokenObtainPairView.as_view(), name='api_token_auth'),
    re_path(r'^api-token-verify/', TokenVerifyView.as_view()),
    re_path(r'^api-token-refresh/', TokenRefreshView.as_view()),
]
