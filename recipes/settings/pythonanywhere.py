from .settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ludde89l$Recipes',
        'USER': 'ludde89l',
        'PASSWORD': 'mysqlpassword35',
        'HOST': 'ludde89l.mysql.pythonanywhere-services.com',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    }
}


ALLOWED_HOSTS = [
    'ludde89l.pythonanywhere.com',
]
