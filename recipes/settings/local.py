from .settings import *

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'recipes',
        'USER': 'root',
        'PASSWORD': 'mysql',
        'HOST': '127.0.0.1',   # Or an IP Address that your DB is hosted on
        'PORT': '13306',
    }
}

ALLOWED_HOSTS.append('10.0.2.2')
ALLOWED_HOSTS.append('localhost')


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
