from django.db import models


class EnumField(models.TextField):
    def __init__(self, enum_class, *args, **kwargs):
        self.enum_class = enum_class
        super().__init__(choices=[(tag, tag.value) for tag in self.enum_class], *args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        kwargs['enum_class'] = self.enum_class
        del kwargs['choices']
        return name, path, args, kwargs

    def to_python(self, value):
        if isinstance(value, self.enum_class):
            return value
        return self.enum_class[value]

    def from_db_value(self, value, expression, connection):
        return self.to_python(value)

    def get_prep_value(self, value):
        return value.name

    def value_to_string(self, obj):
        return self.get_prep_value(obj)
