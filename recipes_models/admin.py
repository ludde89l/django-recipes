from django.contrib import admin

from recipes_models.models import *


class QuantityInline(admin.StackedInline):
    model = Quantity
    extra = 5
    fields = ('amount', 'unit', 'ingredient', 'comment',)


class MomentAdmin(admin.ModelAdmin):
    inlines = [QuantityInline]


class PlannedRecipeInline(admin.StackedInline):
    model = PlannedRecipe
    extra = 5


class DayAdmin(admin.ModelAdmin):
    inlines = [PlannedRecipeInline]


admin.site.register(Recipe)
admin.site.register(Ingredient)
admin.site.register(TimeUnit)
admin.site.register(Category)
admin.site.register(Profile)
admin.site.register(File)
admin.site.register(Moment, MomentAdmin)
admin.site.register(FoodPlanning)
admin.site.register(PlannedDay, DayAdmin)
