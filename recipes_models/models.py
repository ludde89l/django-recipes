from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

from datetime import date
from enum import Enum

from recipes_models import fields


class Ingredient(models.Model):
    name = models.CharField(max_length=30, unique=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'categories'


class File(models.Model):
    file = models.FileField(blank=False, null=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.file.name


class TimeUnit(models.Model):
    name = models.CharField(max_length=20, unique=True)
    seconds = models.IntegerField()

    def sec(self):
        return self.seconds

    def __str__(self):
        return self.name


class Recipe(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True, null=True)
    creator = models.ForeignKey(User, related_name='recipes', null=True, on_delete=models.SET_NULL)
    time = models.FloatField()
    time_unit = models.ForeignKey(TimeUnit, on_delete=models.CASCADE)
    source = models.TextField(blank=True)
    category = models.ManyToManyField(Category)
    portions = models.IntegerField(blank=True, null=True)
    file = models.ForeignKey(File, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'recipes'
        ordering = ['name']

    def owner(self):
        return self.creator

    def __str__(self):
        return self.name


class DisplayType(Enum):
    LST = "List"
    IMG = "Images"

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    favorite_recipes = models.ManyToManyField(Recipe, blank=True)
    display_type = fields.EnumField(enum_class=DisplayType, default=DisplayType.LST)

    def owner(self):
        return self.user

    def __str__(self):
        return self.user.username

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    try:
        instance.profile.save()
    except Profile.DoesNotExist:
        Profile.objects.create(user=instance)


class Moment(models.Model):
    name = models.CharField(max_length=30)
    ingredients = models.ManyToManyField(Ingredient, through='Quantity', related_name="quantities")
    extra_ingredients = models.ManyToManyField(Ingredient, blank=True)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='moments')
    instructions = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.recipe.name + "/" + self.name


class Quantity(models.Model):
    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE)
    moment = models.ForeignKey(Moment, on_delete=models.CASCADE)
    amount = models.DecimalField(decimal_places=2, max_digits=10)
    unit = models.CharField(max_length=10)
    comment = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name_plural = 'quantities'
        constraints = [
            models.UniqueConstraint(
                fields=['ingredient', 'moment'], name='unique_ingredient'
            )
        ]

    def owner(self):
        return self.moment.recipe.owner()

    def __str__(self):
        return "{0} {1} {2}".format(self.amount.normalize(), self.unit, self.ingredient.name)


class FoodPlanning(models.Model):
    owners = models.ManyToManyField(User)

    @property
    def start_date(self):
        return min(self.plannedday_set.all(), key=lambda x: x.date).date

    @property
    def end_date(self):
        return max(self.plannedday_set.all(), key=lambda x: x.date).date

    def __str__(self):
        return f'{self.start_date} - {self.end_date}'


class PlannedDay(models.Model):
    date = models.DateField()
    week = models.ForeignKey(FoodPlanning, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.date}'

    def is_today(self):
        return self.date == date.today()


MEAL_CHOICES = [
    ("B", _("Breakfast")),
    ("L", _("Lunch")),
    ("D", _("Dinner")),
]


class PlannedRecipe(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    day = models.ForeignKey(PlannedDay, on_delete=models.CASCADE)
    meal = models.CharField(max_length=10, choices=MEAL_CHOICES)

    def __str__(self):
        return f'{self.day} {self.meal} {self.recipe.name}'
