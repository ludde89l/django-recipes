import { Component, OnDestroy, OnInit } from '@angular/core';
import { Recipe, RecipeSearch, RecipesService } from '../recipes.service';
import { ReplaySubject, Subscription } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Component({
    selector: 'app-search-page',
    templateUrl: './search-page.component.html',
    styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit, OnDestroy {

    _subscriptions = new Subscription();
    recipeById = new Map<number, Recipe>();
    recipes: Recipe[] = []
    selectedRecipe: Recipe | undefined;
    searchQuery = new ReplaySubject<RecipeSearch>(1);

    constructor(private service: RecipesService) { }

    ngOnInit(): void {

        this._subscriptions.add(
            this.searchQuery.pipe(
                mergeMap((query) => this.service.searchForRecipe$(query))
            ).subscribe((recipes) => this.recipes = recipes)
        )
    }

    doSearch(query: RecipeSearch) {
        this.searchQuery.next(query);
    }

    ngOnDestroy(): void {
        this._subscriptions.unsubscribe();
    }

}
