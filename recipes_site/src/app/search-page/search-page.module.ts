import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchPageComponent } from './search-page.component';
import { RecipesModule } from '../recipes/recipes.module';
import { SearchFormModule } from './search-form/search-form.module';



@NgModule({
  declarations: [SearchPageComponent],
    imports: [
        CommonModule,
        RecipesModule,
        SearchFormModule
    ]
})
export class SearchPageModule { }
