import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { Ingredient, RecipeSearch, RecipesService, User } from '../../recipes.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnDestroy {

    private subscriptions = new Subscription();
    collapsed = false;

    users: User[] = [];

    @Output()
    query = new EventEmitter<RecipeSearch>()

    ingredients: Ingredient[] = [];
    name?: string;
    creator?: string;
    category?: string;
    includingIngredients?: string[];
    excludingIngredients?: string[];

    constructor(private service: RecipesService, private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.subscriptions.add(
            this.service.users$.subscribe((users) => this.users = users)
        );
        this.subscriptions.add(
            this.route.queryParams.subscribe((params: Params) => {
                this.name = params.name;
                this.search();
            })
        );
        this.subscriptions.add(
            this.service.ingredients$.subscribe((ingredients) => this.ingredients = ingredients)
        );
    }

    ngOnDestroy() {
        this.subscriptions.unsubscribe()
    }


    search(): void {
        this.query.emit({
            name: this.name,
            creator: this.creator,
            category: this.category,
            includedIngredients: this.includingIngredients,
            excludedIngredients: this.excludingIngredients
        });
    }
}
