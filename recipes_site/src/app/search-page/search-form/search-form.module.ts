import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchFormComponent } from './search-form.component';
import { FormsModule } from '@angular/forms';



@NgModule({
    declarations: [SearchFormComponent],
    exports: [
        SearchFormComponent
    ],
    imports: [
        CommonModule,
        FormsModule
    ]
})
export class SearchFormModule { }
