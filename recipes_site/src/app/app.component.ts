import { Component, OnDestroy, OnInit } from '@angular/core';
import { EMPTY, empty, filter, flatMap, of, Subscription, timer } from 'rxjs';
import { RecipesService } from './recipes.service';
import { AuthenticationService } from './login-page/authentication.service';
import { catchError, distinctUntilChanged, map, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
    title = 'recipes';

    private _subscriptions = new Subscription();

    constructor(private service: RecipesService, private authenticationService: AuthenticationService) {
    }

    ngOnInit(): void {
        this._subscriptions.add(
            this.authenticationService.isLoggedIn.pipe(
                distinctUntilChanged(),
                mergeMap((loggedIn) => this.loginStatusChanged(loggedIn))
            ).subscribe()
        );
        this._subscriptions.add(
            this.authenticationService.verifyLoginStatus$().subscribe()
        );
        this._subscriptions.add(
            this.service.loadCategories$().subscribe()
        );
        this._subscriptions.add(
            this.service.loadTimeUnits$().subscribe()
        );
        this._subscriptions.add(
            this.service.loadIngredients$().subscribe()
        );
        this._subscriptions.add(
            this.service.loadUsers$().subscribe()
        );
        this._subscriptions.add(
            this.service.getAllRecipes$().subscribe()
        );
        this._subscriptions.add(
            this.authenticationService.authenticationToken.pipe(
                filter((token) => !!token),
                map((token) => this.parseJwt(token)),
                map((jwtObject) => (jwtObject.exp * 1000 - Date.now())),
                filter((millisecondsUntilExpiry) => millisecondsUntilExpiry > 0),
                mergeMap((millisecondsUntilExpiry) => timer(millisecondsUntilExpiry, 1000 * 60 * 5)),
                mergeMap(() => this.authenticationService.refreshToken$())
            ).subscribe()
        );
    }

    parseJwt(token: string) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        return JSON.parse(jsonPayload);
    }

    private loginStatusChanged<A>(loggedIn: A) {
        if(loggedIn) {
            return this.service.loadProfile$();
        }
        this.service.profile$.next(null);
        return EMPTY;
    }

    ngOnDestroy(): void {
        this._subscriptions.unsubscribe();
    }
}
