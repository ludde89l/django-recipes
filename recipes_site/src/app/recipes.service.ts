import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

    profile$ = new BehaviorSubject<Profile | null>(null);
    recipes$ = new ReplaySubject<Recipe[]>(1);
    ingredients$ = new BehaviorSubject<Ingredient[]>([]);
    categories$ = new ReplaySubject<Category[]>(1);
    timeUnits$ = new ReplaySubject<TimeUnit[]>(1);
    users$ = new ReplaySubject<User[]>(1);

    constructor(private http: HttpClient) { }

    getAllRecipes$(): Observable<Recipe[]> {
        return this.http.get<Recipe[]>('/rest/recipes/').pipe(
            tap((data) => this.recipes$.next(data))
        );
    }

    loadProfile$(): Observable<Profile> {
        return this.http.get<Profile>('/rest/myProfile/').pipe(
            tap((profile) => this.profile$.next(profile))
        );
    }

    loadCategories$(): Observable<Category[]> {
        return this.http.get<Category[]>('/rest/categories/').pipe(
            tap((categories) => this.categories$.next(categories))
        );
    }

    loadIngredients$(): Observable<Ingredient[]> {
        return this.http.get<Ingredient[]>('/rest/ingredients/').pipe(
            map((ingredients) => ingredients.map((ingredient) => {
                ingredient.translated_name = upperFirst(ingredient.translated_name);
                return ingredient;
            })),
            tap((ingredients) => this.ingredients$.next(ingredients))
        );
    }

    loadTimeUnits$(): Observable<TimeUnit[]> {
        return this.http.get<TimeUnit[]>('/rest/times/').pipe(
            tap((timeUnits) => this.timeUnits$.next(timeUnits))
        );
    }

    loadUsers$(): Observable<User[]> {
        return this.http.get<User[]>('/rest/users/').pipe(
            tap((timeUnits) => this.users$.next(timeUnits))
        );
    }

    addRecipeAsFavorite$(recipe: Recipe): Observable<Profile | null> {
        return this.http.get<Profile>('/rest/myProfile/').pipe(
            tap((profile) => profile.favorite_recipes.push(recipe.id)),
            tap((profile) => this.profile$.next(profile)),
            mergeMap((profile) => this.http.post<Profile>('/rest/myProfile/', profile)),
            tap((profile) => this.profile$.next(profile)),
            mergeMap(() => this.profile$)
        )
    }



    removeRecipeAsFavorite$(recipe: Recipe): Observable<Profile | null> {
        return this.http.get<Profile>('/rest/myProfile/').pipe(
            tap((profile) => RecipesService.removeFromArray(profile.favorite_recipes, recipe.id)),
            mergeMap((profile) => this.http.post<Profile>('/rest/myProfile/', profile)),
            tap((profile) => this.profile$.next(profile)),
            mergeMap(() => this.profile$)
        )
    }

    searchForRecipe$(searchQuery: RecipeSearch): Observable<Recipe[]> {
        let params = new HttpParams();
        Object.entries(searchQuery).forEach(([key, value]) => {
            if(value) {
                if(Array.isArray(value)) {
                    params = value.reduce((param, element) => param.set(key, element), params)
                }
                else {
                    params = params.set(key, value);
                }
            }
        })
        return this.http.get<Recipe[]>('/rest/recipes/', {params});
    }

    static removeFromArray<T>(array: T[], element: T) {
        const index = array.indexOf(element);
        if (index > -1) {
            array.splice(index, 1);
        }
        return array;
    }

    createRecipe$(formData: FormData): Observable<Recipe> {
        return this.http.post<Recipe>('/rest/upload/', formData);
    }

    createIngredient$(ingredient: CreateIngredient): Observable<Ingredient[]> {
        return this.http.post<Ingredient>('/rest/upload/', ingredient).pipe(
            tap((ingredient) => {
                const addedIngredients = Array.from(this.ingredients$.getValue());
                addedIngredients.push(ingredient);
                this.ingredients$.next(addedIngredients);
            }),
            mergeMap(this.ingredients$.asObservable)
        )
    }
}


function upperFirst(value: string): string {
    if (value.length > 0) {
        return value[0].toUpperCase() + value.substring(1);
    }
    return value;
}

export interface Profile {
    favorite_recipes: number[];
    display_type: string;
}

export interface TimeUnit {
    id: number;
    translated_name: string;
}

export interface User {
    id: number;
    username: string;
}

export interface Category {
    id: number;
    translated_name: string;
}

export interface Ingredient {
    id: number;
    translated_name: string;
}

export interface RecipeIngredient {
    amount: number;
    unit: string;
    ingredient: Ingredient;
    comment: string;
}

export interface Moment {
    id: number;
    name: string;
    ingredients: RecipeIngredient[];
    extra_ingredients: Ingredient[];
    instructions: string;
}

export interface File {
    id: number;
    url: string;
    timestamp: string;
}

export interface Recipe {
    id: number;
    name: string;
    description: string;
    creator: string;
    time: number;
    time_unit: TimeUnit;
    category: Category[];
    moments: Moment[];
    portions: number;
    file: File;
    source: string;
}

export interface CreateIngredient {
    name: string;
}

export interface RecipeSearch {
    name?: string;
    category?: string;
    creator?: string;
    includedIngredients?: string[];
    excludedIngredients?: string[];
}
