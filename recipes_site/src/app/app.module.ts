import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RecipePageModule } from './recipe-page/recipe-page.module';
import { NavBarModule } from './nav-bar/nav-bar.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { RecipesModule } from './recipes/recipes.module';
import { SearchPageModule } from './search-page/search-page.module';
import { LoginPageModule } from './login-page/login-page.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateRecipePageModule } from './create-recipe-page/create-recipe-page.module';
import { RegisterPageModule } from './register-page/register-page.module';
import { AuthInterceptor } from './http-interceptor';

@NgModule({
  declarations: [
    AppComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        RecipePageModule,
        NavBarModule,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        RecipesModule,
        SearchPageModule,
        LoginPageModule,
        BrowserAnimationsModule,
        CreateRecipePageModule,
        RegisterPageModule
    ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule { }
