import { Component, Output, EventEmitter, HostListener, ElementRef, AfterViewInit, ViewChild } from '@angular/core';

@Component({
    selector: 'app-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.scss']
})
export class DialogComponent implements AfterViewInit {

    @ViewChild('dialog', {read: ElementRef, static: true}) dialog!: ElementRef;

    @Output()
    closeRequest = new EventEmitter();

    @HostListener('document:keydown', ['$event'])
    emitCloseRequestOnEsc(event: KeyboardEvent) {
        if (event.key === "Escape") {
            this.emitCloseRequest();
        }
    }

    ngAfterViewInit() {
        this.dialog.nativeElement.focus();
    }

    emitCloseRequest() {
        this.closeRequest.emit();
    }
}
