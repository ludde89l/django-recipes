import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateRecipePageComponent } from './create-recipe-page.component';
import { DialogModule } from '../dialog/dialog.module';
import { CreateRecipeModule } from '../create-recipe/create-recipe.module';



@NgModule({
    declarations: [CreateRecipePageComponent],
    imports: [
        CommonModule,
        DialogModule,
        CreateRecipeModule,
    ]
})
export class CreateRecipePageModule { }
