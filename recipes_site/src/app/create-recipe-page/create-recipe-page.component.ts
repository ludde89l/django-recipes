import { Component, OnInit } from '@angular/core';
import { NewMoment, NewRecipe } from '../create-recipe/create-recipe.component';
import { Category, TimeUnit } from '../recipes.service';

@Component({
  selector: 'app-create-recipe-page',
  templateUrl: './create-recipe-page.component.html',
  styleUrls: ['./create-recipe-page.component.scss']
})
export class CreateRecipePageComponent implements OnInit {

    recipe: NewRecipe = {
        name: "",
        description: "",
        category: [],
        moments: [],
        source: ""
    }

    constructor() { }

    ngOnInit(): void {
    }

}
