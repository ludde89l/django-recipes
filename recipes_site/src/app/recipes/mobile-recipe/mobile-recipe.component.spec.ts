import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileRecipeComponent } from './mobile-recipe.component';

describe('MobileRecipeComponent', () => {
  let component: MobileRecipeComponent;
  let fixture: ComponentFixture<MobileRecipeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileRecipeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
