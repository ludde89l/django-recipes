import { Component, Input } from '@angular/core';

import { Recipe } from '../../recipes.service';
import { Router } from '@angular/router';

@Component({
    selector: 'mobile-recipe',
    templateUrl: './mobile-recipe.component.html',
    styleUrls: ['./mobile-recipe.component.scss']
})
export class MobileRecipeComponent {

    @Input()
    selectedRecipe: Recipe | undefined;

    constructor(private router: Router) {
    }

    closeDialog() {
        this.router.navigate(['/']);
    }
}
