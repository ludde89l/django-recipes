import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Moment } from '../../../recipes.service';

@Component({
  selector: 'app-moment-tab',
  templateUrl: './moment-tab.component.html',
  styleUrls: ['./moment-tab.component.scss']
})
export class MomentTabComponent implements OnInit {

    @Input()
    moment!: Moment;

    @Input()
    active!: boolean;

    @Output()
    click = new EventEmitter();

    constructor() { }

    ngOnInit(): void {

    }

}
