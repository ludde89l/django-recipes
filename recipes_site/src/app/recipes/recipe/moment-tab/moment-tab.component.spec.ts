import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MomentTabComponent } from './moment-tab.component';

describe('MomentTabComponent', () => {
  let component: MomentTabComponent;
  let fixture: ComponentFixture<MomentTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MomentTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MomentTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
