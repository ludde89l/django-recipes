import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Moment, Recipe } from '../../../recipes.service';

@Component({
  selector: 'app-moment',
  templateUrl: './moment.component.html',
  styleUrls: ['./moment.component.scss']
})
export class MomentComponent implements OnChanges {

    @Input()
    moment!: Moment;

    @Input()
    recipe!: Recipe;

    extraIngredients: string | null = null;

    ngOnChanges(): void {
        if(this.moment.extra_ingredients) {
            this.extraIngredients = this.moment.extra_ingredients.reduce((prev, cur) => prev + cur.translated_name + " ", "")
        }
    }

}
