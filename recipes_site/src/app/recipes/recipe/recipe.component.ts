import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Moment, Profile, Recipe, RecipesService } from '../../recipes.service';
import { of, Subscription } from 'rxjs';
import { AuthenticationService } from '../../login-page/authentication.service';
import { mergeMap, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit, OnDestroy, OnChanges {

    @Input()
    recipe!: Recipe;

    favorite: boolean = false;
    profile: Profile | null = null;

    selectedMoment: Moment | null = null;

    private _subscriptions = new Subscription();

    constructor(private authenctioncationService: AuthenticationService,
    private service: RecipesService, private router: Router) {
    }

    ngOnInit(): void {
        this._subscriptions.add(
            this.authenctioncationService.isLoggedIn.pipe(
                mergeMap((loggedIn) => loggedIn ? this.service.profile$.pipe(
                    tap((profile) => {
                        this.profile = profile;
                        this.favorite = !!profile?.favorite_recipes.includes(this.recipe.id)
                    })
                ) : of())
            ).subscribe()
        );
        this.selectedMoment = this.recipe.moments[0];
    }

    ngOnChanges() {
        if(this.profile) {
            this.favorite = this.profile.favorite_recipes.includes(this.recipe.id);
        }
        this.selectedMoment = this.recipe.moments[0];
    }

    flipHeart() {
        if(this.favorite) {
            this._subscriptions.add(
                this.service.removeRecipeAsFavorite$(this.recipe).subscribe()
            );
        }
        else {
            this._subscriptions.add(
                this.service.addRecipeAsFavorite$(this.recipe).subscribe()
            );
        }
    }

    ngOnDestroy(): void {
        this._subscriptions.unsubscribe();
    }

    selectMoment(moment: Moment) {
        this.selectedMoment = moment;
    }

    copyRecipeUrl() {
        const path = this.router.createUrlTree(['/recipe', this.recipe.id]).toString();
        let shareUrl = window.location.origin + path;
        if(navigator.share) {
            navigator.share({
                title: 'Recept på ' + this.recipe.name,
                text: 'Här är ett gott recept på ' + this.recipe.name,
                url: shareUrl
            })
                .then(() => console.log('Share complete'))
                .catch((error) => console.error('Could not share at this time', error))
        }
        else {
            navigator.clipboard.writeText(shareUrl);
        }
    }
}
