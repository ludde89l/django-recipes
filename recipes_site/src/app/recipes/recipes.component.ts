import { Component, HostListener, Input, EventEmitter, Output } from '@angular/core';
import { Recipe } from '../recipes.service';

@Component({
    selector: 'app-recipes',
    templateUrl: './recipes.component.html',
    styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent {

    @Input()
    recipes: Recipe[] = []
    @Input()
    selectedRecipe: Recipe | undefined;
    isMobile = false;

    @Output()
    recipeSelected = new EventEmitter<Recipe>()

    ngOnInit(): void {
        this.isMobile = window.innerWidth < 768;
    }

    @HostListener('window:resize', ['$event'])
    onResize(event: any) {
        this.isMobile = event.target.innerWidth < 768;
    }
}
