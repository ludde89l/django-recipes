import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Recipe } from '../../recipes.service';

@Component({
  selector: 'app-recipe-list-item',
  templateUrl: './recipe-list-item.component.html',
  styleUrls: ['./recipe-list-item.component.scss']
})
export class RecipeListItemComponent {

    @Input()
    active!: boolean;

    @Input()
    recipe: Recipe | null = null;

    @Output()
    click = new EventEmitter();
}
