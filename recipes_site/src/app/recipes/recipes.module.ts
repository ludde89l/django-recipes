import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipesComponent } from './recipes.component';
import { RecipeListItemComponent } from './recipe-list-item/recipe-list-item.component';
import { RecipeComponent } from './recipe/recipe.component';
import { MobileRecipeComponent } from './mobile-recipe/mobile-recipe.component';
import { MomentTabComponent } from './recipe/moment-tab/moment-tab.component';
import { MomentComponent } from './recipe/moment/moment.component';
import { DialogModule } from '../dialog/dialog.module';
import { RouterModule } from '@angular/router';



@NgModule({
    declarations: [RecipesComponent, RecipeListItemComponent, RecipeComponent, MobileRecipeComponent, MomentTabComponent, MomentComponent],
    exports: [
        RecipesComponent
    ],
    imports: [
        CommonModule,
        DialogModule,
        RouterModule
    ]
})
export class RecipesModule { }
