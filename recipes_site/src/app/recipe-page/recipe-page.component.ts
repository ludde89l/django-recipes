import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Recipe, RecipesService } from '../recipes.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../login-page/authentication.service';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-recipe-page',
  templateUrl: './recipe-page.component.html',
  styleUrls: ['./recipe-page.component.scss']
})
export class RecipePageComponent implements OnInit, OnDestroy {

    _subscriptions = new Subscription();
    recipeById = new Map<number, Recipe>();
    recipes: Recipe[] = []
    selectedRecipe: Recipe | undefined;
    selectedRecipeId: number  | null | undefined = undefined;
    favoriteRecipes: number[] = [];
    loggedIn: boolean = true;

    constructor(private service: RecipesService,
                private authenticationService: AuthenticationService,
                private route: ActivatedRoute,
                private router: Router) { }

    ngOnInit(): void {
        this._subscriptions.add(
            this.service.profile$.subscribe((data) => {
                this.favoriteRecipes = data?.favorite_recipes || [];
                this.recipes = this.findFavoriteRecipes();
            })
        );
        this._subscriptions.add(
            this.authenticationService.isLoggedIn
                .pipe(
                    distinctUntilChanged()
                ).subscribe((loggedIn) => {
                this.loggedIn = loggedIn;
                this.checkLoggedInStatus();
            })
        );
        this._subscriptions.add(
            this.service.recipes$.subscribe((data) => {
                data.forEach((recipe) => this.recipeById.set(recipe.id, recipe));
                this.recipes = this.findFavoriteRecipes();
                this.selectedRecipe = this.findSelectedRecipe();
            })
        )
        this._subscriptions.add(
            this.route.paramMap.subscribe(params => {
                const paramId = params.get('id');
                if(paramId) {
                    this.selectedRecipeId = parseInt(paramId, 10);
                    this.selectedRecipe = this.findSelectedRecipe();
                }
                else {
                    this.selectedRecipeId = null;
                }
                this.checkLoggedInStatus();
            })
        );
    }

    checkLoggedInStatus() {
        if(!this.loggedIn && this.selectedRecipeId === null) {
            this.router.navigate(['/login'])
        }
    }

    findSelectedRecipe(): Recipe | undefined {
        if(this.selectedRecipeId) {
            return this.recipeById.get(this.selectedRecipeId);
        }
        return undefined;
    }

    findFavoriteRecipes(): Recipe[] {
        return this.favoriteRecipes
            .map((recipeId) => this.recipeById.get(recipeId))
            .filter((recipe) => !!recipe) as Recipe[];
    }

    ngOnDestroy(): void {
        this._subscriptions.unsubscribe();
    }

    selectRecipe(recipe: Recipe) {
        this.router.navigate(['/recipe', recipe.id]);
    }
}
