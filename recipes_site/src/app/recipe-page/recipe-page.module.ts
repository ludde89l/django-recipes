import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { RecipePageComponent } from './recipe-page.component';
import { NavBarModule } from '../nav-bar/nav-bar.module';
import { RouterModule } from '@angular/router';
import { RecipesModule } from '../recipes/recipes.module';

@NgModule({
    declarations: [
        RecipePageComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        NavBarModule,
        RouterModule,
        RecipesModule
    ],
    providers: [],
    exports: [
        RecipePageComponent
    ],
    bootstrap: []
})
export class RecipePageModule { }
