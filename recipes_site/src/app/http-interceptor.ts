import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { AuthenticationService } from './login-page/authentication.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private auth: AuthenticationService, private router: Router) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Get the auth token from the service.
        const authToken = this.auth.authenticationToken.value;
        let headers = req.headers;
        if(authToken) {
            headers = headers.set('Authorization', "Bearer " + authToken)
        }
        let cookie = AuthInterceptor.getCookie('csrftoken');
        if(cookie) {
            headers = headers.set('X-CSRFToken', cookie);
        }
        const authReq = req.clone({
            headers
        });

        // send cloned request with header to the next handler.
        return next.handle(authReq).pipe(
            tap((event) => {

                if (event instanceof HttpResponse) {
                    if(event.status === 401) {
                        this.auth.logout();
                        this.router.navigate(['login'])
                    }
                }
                return event;
            })
        );
    }

    private static getCookie(name: string): string | null {
        let cookieValue: string | null = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
}
