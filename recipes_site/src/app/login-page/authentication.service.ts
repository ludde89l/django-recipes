import { Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, Observable, of, ReplaySubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

    isLoggedIn = new ReplaySubject<boolean>(1);
    authenticationToken = new BehaviorSubject<string>("");
    private refreshToken?: string;

    constructor(private http: HttpClient) {}

    login$(username: string, password: string): Observable<LoginResult> {
        return this.http.post<LoginToken>('/rest/api-token-auth/', {username, password}).pipe(
            tap((data) => this.setJWTToken(data)),
            map(() => {return {success: true}}),
            catchError((message) => of(AuthenticationService.createLoginResultFromFailure(message.error)))
        )
    }

    refreshToken$(): Observable<any> {
        return this.http.post<LoginToken>('/rest/api-token-refresh/', {refresh: this.refreshToken}).pipe(
            tap((data) => this.setJWTToken(data)),
            map(() => {return {success: true}}),
            catchError((message) => {
                this.isLoggedIn.next(false);
                return of(AuthenticationService.createLoginResultFromFailure(message.error))
            })
        )
    }

    verifyLoginStatus$(): Observable<any> {
        const jwtKey = localStorage.getItem("JWTKey");
        if(jwtKey) {
            this.refreshToken = jwtKey;
        } else {
            this.isLoggedIn.next(false);
            return EMPTY;
        }
        return this.refreshToken$();
    }

    private static createLoginResultFromFailure(error: LoginError): LoginResult {
        let errorMessage = undefined;
        if(error.non_field_errors) {
            errorMessage = error.non_field_errors[0];
        }
        return {success: false, errorMessage}
    }

    setJWTToken(token: LoginToken) {
        this.authenticationToken.next(token.access);
        if(token.refresh) {
            this.refreshToken = token.refresh;
            localStorage.setItem("JWTKey", token.refresh);
        }
        this.isLoggedIn.next(true);
    }

    logout() {
        this.authenticationToken.next("");
        this.isLoggedIn.next(false);
        localStorage.removeItem("JWTKey");
    }
}

export interface LoginToken {
    refresh?: string;
    access: string;
}

export interface LoginResult {
    success: boolean;
    errorMessage?: string;
}

interface LoginError {
    non_field_errors?: string[];
}
