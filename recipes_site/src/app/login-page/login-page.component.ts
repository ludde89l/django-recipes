import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthenticationService, LoginResult } from './authentication.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {

    private _subscriptions = new Subscription();

    username: string = "";
    password: string = "";
    errorMessage?: string;

    constructor(private service: AuthenticationService, private router: Router) {
    }

    ngOnInit() {
        this._subscriptions.add(
            this.service.isLoggedIn.subscribe((loggedIn) => {
                if(loggedIn) {
                    this.router.navigate(['']);
                }
            })
        )
    }

    login() {
        this._subscriptions.add(
            this.service.login$(this.username, this.password).subscribe((result) => this.handleLoginResult(result))
        );
    }

    handleLoginResult(result: LoginResult) {
        if(result.success) {
            this.router.navigate(['/']);
        }
        else {
            this.errorMessage = result.errorMessage || "Du kunde tyvärr inte loggas in just nu, försök igen senare";
        }
    }

    ngOnDestroy(): void {
        this._subscriptions.unsubscribe();
    }
}
