import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { AuthenticationService, LoginToken } from '../login-page/authentication.service';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnDestroy {

    private subscriptions = new Subscription();

    username: string = "";
    _password: string = "";
    _confirmPassword: string = "";
    email: string = "";

    validPassword = true;

    constructor(private httpClient: HttpClient, private service: AuthenticationService, private router: Router) {
    }

    set confirmPassword(password: string) {
        this._confirmPassword = password;
        if(this._password === this._confirmPassword) {
            this.validPassword = true;
        }
    }

    get confirmPassword() {
        return this._confirmPassword;
    }

    set password(password: string) {
        this._password = password
        if(this._password === this._confirmPassword) {
            this.validPassword = true;
        }
    }

    get password() {
        return this._password;
    }

    registerUser() {
        if(this.password !== this.confirmPassword) {
            this.validPassword = false;
            return;
        }

        this.subscriptions.add(
            this.httpClient.post<LoginToken>('/rest/users/', {username: this.username, password: this._password, email: this.email}).pipe(
                tap((token) => this.service.setJWTToken(token))
            ).subscribe(() => this.router.navigate(['/']))

        );
    }

    ngOnDestroy() {
        this.subscriptions.unsubscribe();
    }
}
