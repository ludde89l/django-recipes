import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthenticationService } from '../login-page/authentication.service';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Recipe, RecipesService } from '../recipes.service';
import { map, startWith } from 'rxjs/operators';
import { UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit, OnDestroy {

    private _subscriptions = new Subscription();

    loggedIn: boolean = false;
    collapsed = true;
    searchQuery = "";
    recipes: Recipe[] = [];
    filteredOptions!: Observable<Recipe[]>;
    myControl = new UntypedFormControl();


    constructor(private service: AuthenticationService, private recipeService: RecipesService, private router: Router) { }

    ngOnInit(): void {
        this._subscriptions.add(
            this.service.isLoggedIn.subscribe((loggedIn) => this.loggedIn = loggedIn)
        );
        this._subscriptions.add(
            this.recipeService.recipes$.subscribe((recipes) => this.recipes = recipes)
        );
        this.filteredOptions = this.myControl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
        );
    }

    private _filter(value: string): Recipe[] {
        const filterValue = value.toLowerCase();

        return this.recipes.filter((recipe) => recipe.name.toLowerCase().indexOf(filterValue) === 0);
    }

    ngOnDestroy(): void {
        this._subscriptions.unsubscribe();
    }

    toggleCollapsed() {
        this.collapsed = !this.collapsed;
    }

    search() {
        this.toggleCollapsed();
        this.router.navigate(['search'], {queryParams: {name: this.myControl.value}})
    }

    selectRecipe(recipe: Recipe) {
        this.toggleCollapsed();
        this.router.navigate(['recipe', recipe.id]);
    }
}
