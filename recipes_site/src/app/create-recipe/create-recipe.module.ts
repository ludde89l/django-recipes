import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateRecipeComponent } from './create-recipe.component';
import { CreateMomentComponent } from './create-moment/create-moment.component';
import { AddIngredientComponent } from './create-moment/add-ingredient/add-ingredient.component';
import { FormsModule } from '@angular/forms';
import { DialogModule } from '../dialog/dialog.module';
import { CreateIngredientComponent } from './create-moment/add-ingredient/create-ingredient/create-ingredient.component';



@NgModule({
    declarations: [CreateRecipeComponent, CreateMomentComponent, AddIngredientComponent, CreateIngredientComponent],
    exports: [
        CreateRecipeComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        DialogModule
    ]
})
export class CreateRecipeModule { }
