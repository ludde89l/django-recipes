import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Category, Ingredient, Moment, RecipesService, TimeUnit } from '../recipes.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-recipe',
  templateUrl: './create-recipe.component.html',
  styleUrls: ['./create-recipe.component.scss']
})
export class CreateRecipeComponent implements OnInit, OnDestroy {

    private _subscriptions = new Subscription();

    @Input()
    recipe!: NewRecipe;
    categories!: Category[];
    timeUnits!: TimeUnit[];

    constructor(private service: RecipesService, private router: Router) {
    }

    ngOnInit() {
        this._subscriptions.add(
            this.service.categories$.subscribe((categories) => this.categories = categories)
        );
        this._subscriptions.add(
            this.service.timeUnits$.subscribe((timeUnits) => {
                this.timeUnits = timeUnits;
                this.recipe.time_unit = timeUnits[0];
            })
        );
    }

    createRecipe() {
        const formData = new FormData();
        formData.append("json", JSON.stringify(this.recipe));
        const selectedFileList = (<HTMLInputElement>document.getElementById('url')).files;
        if(selectedFileList && selectedFileList.length) {
            const file = selectedFileList.item(0);
            if(file) {
                formData.append("file", file);
            }
        }
        this._subscriptions.add(
            this.service.createRecipe$(formData).subscribe((recipe) => this.router.navigate(['recipe', recipe.id]))
        );
    }

    addMoment() {
        this.recipe.moments.push({
            name: "",
            ingredients: [],
            extra_ingredients: [],
            instructions: ""
        })
    }

    ngOnDestroy() {
        this._subscriptions.unsubscribe();
    }
}

export interface NewRecipe {
    name: string;
    description: string;
    time?: number;
    time_unit?: TimeUnit;
    category: Category[];
    moments: NewMoment[];
    portions?: number;
    file?: File;
    source: string;
}

export interface NewMoment {
    name: string;
    ingredients: NewRecipeIngredient[];
    extra_ingredients: Ingredient[];
    instructions: string;
}

export interface NewRecipeIngredient {
    amount?: number;
    unit: string;
    ingredient?: Ingredient;
    comment: string;
}
