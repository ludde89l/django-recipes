import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Ingredient, RecipesService } from '../../recipes.service';
import { NewMoment } from '../create-recipe.component';

@Component({
  selector: 'app-create-moment',
  templateUrl: './create-moment.component.html',
  styleUrls: ['./create-moment.component.scss']
})
export class CreateMomentComponent implements OnInit, OnDestroy {

    private _subscriptions = new Subscription();

    @Input()
    moment!: NewMoment;

    ingredients!: Ingredient[];

    constructor(private service: RecipesService) {
    }

    ngOnInit(): void {
        this._subscriptions.add(
            this.service.ingredients$.subscribe((ingredients) => this.ingredients = ingredients)
        );
    }

    ngOnDestroy() {
        this._subscriptions.unsubscribe();
    }

    addIngredient() {
        this.moment.ingredients.push({
            amount: 0,
            unit: "",
            ingredient: undefined,
            comment: ""
        })
    }
}
