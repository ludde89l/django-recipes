import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Ingredient, RecipesService } from '../../../recipes.service';
import { Subscription } from 'rxjs';
import { NewRecipeIngredient } from '../../create-recipe.component';

@Component({
  selector: 'app-add-ingredient',
  templateUrl: './add-ingredient.component.html',
  styleUrls: ['./add-ingredient.component.scss']
})
export class AddIngredientComponent implements OnInit, OnDestroy {

    private _subscriptions = new Subscription();

    @Input()
    selectedIngredient!: NewRecipeIngredient;

    ingredients!: Ingredient[];

    showCreateIngredient = false;

    constructor(private service: RecipesService) {
    }

    ngOnInit(): void {
        this._subscriptions.add(
            this.service.ingredients$.subscribe((ingredients) => this.ingredients = ingredients)
        );
    }

    ngOnDestroy() {
        this._subscriptions.unsubscribe();
    }

    createIngredient() {
        this.showCreateIngredient = true;
    }
}
