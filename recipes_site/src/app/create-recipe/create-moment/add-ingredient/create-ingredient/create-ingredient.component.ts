import { Component, Output, EventEmitter, OnDestroy } from '@angular/core';
import { RecipesService } from '../../../../recipes.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create-ingredient',
  templateUrl: './create-ingredient.component.html',
  styleUrls: ['./create-ingredient.component.scss']
})
export class CreateIngredientComponent implements OnDestroy {
    recipeName: string = "";

    private _subscriptions = new Subscription();

    @Output()
    close = new EventEmitter();

    constructor(private service: RecipesService) {
    }

    saveIngredients() {
        this._subscriptions.add(
            this.service.createIngredient$({
                name: this.recipeName
            }).subscribe(() => this.closeModal())
        );
    }

    closeModal() {
        this.close.emit();
    }

    ngOnDestroy() {
        this._subscriptions.unsubscribe();
    }
}
