import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipePageComponent } from './recipe-page/recipe-page.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { CreateRecipePageComponent } from './create-recipe-page/create-recipe-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';

const routes: Routes = [
    { path: '', component: RecipePageComponent },
    { path: 'recipe', component: RecipePageComponent },
    { path: 'search', component: SearchPageComponent },
    { path: 'search/:id', component: SearchPageComponent },
    { path: 'login', component: LoginPageComponent },
    { path: 'register', component: RegisterPageComponent },
    { path: 'recipe/:id', component: RecipePageComponent },
    { path: 'createRecipe', component: CreateRecipePageComponent },
    { path: 'register', component: RegisterPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
