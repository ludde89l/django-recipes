module.exports = {
  entry: [
    "./static/recipes_site/react/index.js",
  ],
  output: {
    path: __dirname + '/static/recipes_site/js',
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015','react','stage-2']
        },
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
  ]
};

