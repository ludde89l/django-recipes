from django.urls import re_path
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static

from recipes_site import views


app_name = "recipes_site"
urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + [
    re_path(r'^ngsw-worker.js', TemplateView.as_view(
        template_name="recipes_site/ngsw-worker.js",
        content_type='application/javascript',
    ), name='ngsw-worker.js'),
    re_path(r'^ngsw.json', TemplateView.as_view(
        template_name="recipes_site/ngsw.json",
        content_type='application/json',
    ), name='ngsw-worker.js'),
    re_path(r'^$', views.index),
    re_path(r'^recipe/[0-9]*$', views.index),
    re_path(r'^search/[0-9]*$', views.index),
    re_path(r'^login$', views.index),
    re_path(r'^register$', views.index),
    re_path(r'^createRecipe$', views.index),
]
