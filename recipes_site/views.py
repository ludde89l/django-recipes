from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.http import JsonResponse
from django.views.generic import FormView
from django.contrib.auth import login
from django.contrib.auth.models import User

from bs4 import BeautifulSoup
import requests
import json

from recipes_models.models import Profile, Recipe, Category, Ingredient, DisplayType, FoodPlanning
from recipes_site.forms import EditUserForm


def index(request, *args, **kwargs):
    return render(request, 'recipes_site/index.html')


def file_view(request, filename):
    return render(request, 'recipes_site/' + filename)


@login_required
def create_recipe(request):
    return render(request, 'recipes_site/addRecipe.html')


@login_required
def import_recipe(request):
    return render(request, 'recipes_site/addIcaRecipe.html')


@login_required
def category(request, category):
    context = {
        'category': category
    }
    return render(request, 'recipes_site/category.html', context=context)


@login_required
def recipe_import(request, id):
    result = requests.get("https://handla.api.ica.se/api/recipes/recipe/{0}/".format(id), headers={'Accept': 'application/json'})
    json = result.json()
    for i, val in enumerate(json['CookingSteps']):
        json['CookingSteps'][i] = BeautifulSoup(val, "html.parser").text
    return JsonResponse(json)


@login_required
def event_planning(request, id):
    context = {
        'week_plan': FoodPlanning.objects.get(id=id)
    }
    return render(request, 'recipes_site/foodPlanning.html', context=context)


@login_required
def change_favorite(request):
    body = json.loads(request.body)
    recipe = Recipe.objects.get(id=body['recipe'])
    profile = Profile.objects.get(user=request.user)
    if body['favorite']:
        profile.favorite_recipes.add(recipe)
    else:
        profile.favorite_recipes.remove(recipe)
    profile.save()
    return JsonResponse({})


@login_required
def create_food_plan(request):
    return render(request, 'recipes_site/createFoodPlanning.html')


def recipe(request, id):
    if request.user.is_authenticated:
        (profile, _) = Profile.objects.get_or_create(user=request.user)
        recipes = profile.favorite_recipes.all() | Recipe.objects.filter(creator=request.user).distinct()
    else:
        recipes = []
        profile = None
    context = {
        'recipe_list': recipes,
        'display_types': [enum for enum in DisplayType],
        'profile': profile,
        'recipe': '/rest/recipes/{0}'.format(id)
    }
    return render(request, 'recipes_site/index.html', context)


class RegisterView(FormView):
    template_name = 'registration/register.html'
    form_class = UserCreationForm

    def get_success_url(self):
        return reverse('recipes_site.index')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('recipes_site:index')
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.save())
        return super().form_valid(form)


class EditUserView(FormView):
    template_name = 'recipes_site/profile.html'
    form_class = EditUserForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = self.request.user
        context['display_types'] = (value for _, value in DisplayType.__members__.items())
        return context
